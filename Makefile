proj_path := $(dir $(realpath $(firstword $(MAKEFILE_LIST))))

build: clean
	staticjinja build --outpath=$(proj_path)public/
	rsync -av $(proj_path)assets/ $(proj_path)public/

clean:
	rm -fr $(proj_path)public/*

watch:
	while true; do \
		make $(WATCHMAKE); \
		inotifywait -qre close_write .; \
	done

